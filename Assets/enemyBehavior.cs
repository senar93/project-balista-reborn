﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class enemyBehavior : PooledObject {

    public float startingSpeed = 0.5f;
    public float startingFrequency = 0.5f;
    public float startingMagnitude =0.7f;

    private float MoveSpeed;
    private float frequency;  // Speed of sine movement
    private float magnitude;   

    private Vector3 pos;
    // Update is called once per frame
    void Update () {
        pos += Vector3.left * Time.deltaTime * MoveSpeed/3f;
        transform.position = pos + Vector3.up * Mathf.Sin(Time.time * frequency) * magnitude;

        if(transform.position.x <-15)
            ReturnToPool();
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            ReturnToPool();
        }
    }
 
 

    public void Reset()
    {
        pos = new Vector3(15, 3 + Random.Range(0, 6), 2);
        transform.position = pos;

        MoveSpeed = startingSpeed + Random.Range(0, 1);
        frequency = Random.Range(1f, 3f);
        magnitude = startingMagnitude - Random.Range(0, 0.5f);
    }
}
