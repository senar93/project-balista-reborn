﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class levelManager : MonoBehaviour {
    public enemyBehavior enemy;
    public float amountToWait = 5;
    private float timer;

    private float difficultyTimer;
	// Use this for initialization
	void Start () {
       

    }

    // Update is called once per frame
    void Update() {

        if (amountToWait > 1) { 
            difficultyTimer += Time.deltaTime;
            if (difficultyTimer >= 10)
            {
                difficultyTimer -= 10;
                amountToWait -= Random.Range(0.1f, 1f);
                if (amountToWait < 1)
                    amountToWait = 1;
            }
        }

        timer += Time.deltaTime;
        if(timer >= amountToWait)
        {
            timer -= amountToWait;
            enemyBehavior newEnemy = enemy.GetPooledInstance<enemyBehavior>();
            newEnemy.Reset();
        }
	}
}
