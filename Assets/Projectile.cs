﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour {
    Vector3 startPos;
    // Use this for initialization

    bool flying = false;
    bool isSelected = false;

    float range = 2;
    float strength = 7;

    Rigidbody2D body;
	void Start () {
        startPos= this.transform.position;
        Debug.Log(startPos);
        body= gameObject.GetComponent<Rigidbody2D>();
        body.isKinematic = true;
    }

    Vector3 initialPos;
    void handleTouch()
    {
        Touch touch = Input.GetTouch(0);

        Vector3 touchPosition = Camera.main.ScreenToWorldPoint(touch.position);

        // Handle finger movements based on touch phase.
        switch (touch.phase)
        {
            // Record initial touch position.
            case TouchPhase.Began:
                //checkCollision(touchPosition);
                initialPos = touchPosition;
                break;

            // Determine direction by comparing the current touch position with the initial one.
            case  TouchPhase.Moved:

                //if (isSelected) {                   
                    Vector3 newPosition = startPos+ (touchPosition-initialPos);

                    Vector3 distFromStart = newPosition - startPos;

                    if (distFromStart.magnitude >= range) { 
                        distFromStart.Normalize();
                        this.transform.position = startPos+ range*distFromStart;
                    }
                    else
                        this.transform.position = newPosition;                
                //}
                break;

            // Report that a direction has been chosen when the finger is lifted.
            case TouchPhase.Ended:

                Vector3 dist = startPos - this.transform.position;
                if (dist.magnitude > 0.2f)
                {
                    flying = true;
                    body.bodyType = RigidbodyType2D.Dynamic;
                    isSelected = false;
                    body.AddForce(dist * strength, ForceMode2D.Impulse);
                }

                break;
        }
    }

    void checkCollision(Vector3 touchPositionInWorldSpace)
    {
            RaycastHit2D hit = Physics2D.Raycast(touchPositionInWorldSpace, Vector2.zero);
            if (hit.collider != null)
            {
                if (hit.transform.Equals(this.transform))
                {
                    isSelected = true;
                }
            }
        }

    // Update is called once per frame
    void Update()
    {

        if (flying == false)
        {
            if (Input.touchCount > 0)
            {
                handleTouch();
            }
        }else if(this.transform.position.y < -2)
        {
            this.transform.position = startPos;
            body.bodyType = RigidbodyType2D.Static;
            flying = false;
        }
    }
    
}
