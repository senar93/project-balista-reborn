﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dato : PooledObject
{
    Vector3 position;
    void Start()
    {
        position = new Vector3(0, 0, 0);
    }

    private void FixedUpdate()
    {
        position.x += 5 * Time.deltaTime;
        transform.position= position;

        if (position.x > 30)
            ReturnToPool();
    }

    public void resetPosition()
    {
        position.x = 0;
    }
}
